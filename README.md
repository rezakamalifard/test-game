# Welcome to Battle Game!
This is a simple game to server used to
- Create a player
- Submit battle requests to battle processor
- Retrieve leaderboard
## Requirements
Check the provided ```requirements.txt``` file for needed packages to run this server

You can install requirements using
```shell script
pip install -r requirements.txt
```
## Run tests
```shell script
python test_app.py
```
## Usage
You can use .flaskenv file to set application environment
In order to use that we need to install ```python-dotenv```
```shell script
pip install python-dotenv
``` 
Then Run Flask Server
```shell script
flask run
```
and the ```battle processor worker```
```shell script
python battle_processor_worker.py
```

## List of suggested improvements
- Restructure the project move each app's tests to it's directory
- Use a cache to store leaderboard response or in memory data structure like a Redis Sorted set to store and retrieve the leaderboard
- Use an async Queue like a Redis Queue for battle processing queue
- Add a runner script to setup and run the application
- Create a battle engine to call it from the battle processor queue
- Use locks to impalement concurrent execution of non-conflicting battles
- Use validators based on WTF to validate use request data
- Log the battle data using logger
- Add tests for Battle views

## Production ready application
- Create a Dockerfile for the application
- CI/CD Pipeline to test and deploy the application
- Use MySQL or PostgreSQL as the database
- Add logger to the app

