from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config

db = SQLAlchemy()
migrate = Migrate()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)

    from app.players import bp as players_bp
    app.register_blueprint(players_bp, url_prefix='/players')

    from app.battles import bp as battles_bp
    app.register_blueprint(battles_bp, url_prefix='/battles')

    # TODO: Config the logger

    return app


from app.players import models
from app.battles import models
