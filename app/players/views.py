from flask import request, escape, json

from app import db
from app.players import bp
from app.players.models import Player
from errors import bad_request, error_response


@bp.route('', methods=['POST'])
def create_player():
    name_field_max_lenght = 20
    if not request.data:
        return bad_request('request body is empty')
    data = json.loads(request.data)
    if 'name' not in data:
        return bad_request('request body must include name field')
    name = data['name'].lower()
    data['name'] = name
    if len(name) > name_field_max_lenght:
        return bad_request(f'max lenght for name field is {name_field_max_lenght}')
    if Player.query.filter_by(name=name).first():
        return bad_request(f'{escape(name)} is already exists')
    player = Player()
    player.from_dict(data)
    db.session.add(player)
    db.session.commit()
    return player.to_dict()


@bp.route('/<int:player_id>', methods=['GET'])
def get_player(player_id):
    player = Player.query.get(player_id)
    if not player:
        return error_response(404, f'no player with player_id={escape(player_id)}')
    return player.to_dict()


@bp.route('/leaderboard', methods=['GET'])
def get_leaderboard():
    resp = {}
    players = Player.query.order_by(Player.gold_amount.desc()).all()
    rank = 1
    for player in players:
        resp[rank] = player.to_dict()
        rank += 1
    return resp
