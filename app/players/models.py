from sqlalchemy import func

from app import db


class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), index=True, unique=True)
    gold_amount = db.Column(db.Integer, index=True, default=0)
    attack_value = db.Column(db.Integer, default=0)
    hit_points = db.Column(db.Integer, default=0)
    luck_value = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=func.now())

    attacks = db.relationship('Battle',
                              foreign_keys='Battle.attacker_id',
                              backref='attacker', lazy='dynamic')
    defends = db.relationship('Battle',
                              foreign_keys='Battle.defender_id',
                              backref='defender', lazy='dynamic')

    wins = db.relationship('Battle',
                           foreign_keys='Battle.winner_id',
                           backref='winner', lazy='dynamic')

    def __repr__(self):
        return f'<Player {self.name}>'

    def from_dict(self, data):
        for field in ['name', 'gold_amount', 'attack_value', 'hit_points',
                      'luck_value']:
            if field in data:
                setattr(self, field, data[field])

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'gold_amount': self.gold_amount,
            'attack_value': self.attack_value,
            'hit_points': self.hit_points,
            'luck_value': self.luck_value,
            'created_at': self.created_at.isoformat() + 'Z'
        }
