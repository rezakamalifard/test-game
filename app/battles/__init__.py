from flask import Blueprint

bp = Blueprint('battles', __name__)

from app.battles import views
