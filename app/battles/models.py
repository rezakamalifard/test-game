from sqlalchemy import func

from app import db


class Battle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    attacker_id = db.Column(db.Integer, db.ForeignKey('player.id'))
    defender_id = db.Column(db.Integer, db.ForeignKey('player.id'))
    winner_id = db.Column(db.Integer, db.ForeignKey('player.id'))
    started = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, default=func.now())

    def __repr__(self):
        return f'<Battle Between {self.attacker_id} and {self.defender_id}>'

    def from_dict(self, data):
        for field in ['attacker_id', 'defender_id']:
            if field in data:
                setattr(self, field, data[field])

    def to_dict(self):
        return {
            'id': self.id,
            'attacker_id': self.attacker_id,
            'defender_id': self.defender_id,
            'winner_id': self.winner_id,
            'created_at': self.created_at.isoformat() + 'Z',
            'started': self.started
        }
