from flask import request, json

from app import db
from app.battles import bp
from app.battles.models import Battle
from app.players.models import Player
from errors import bad_request


@bp.route('', methods=['POST'])
def create_battle():
    if not request.data:
        return bad_request('request body is empty')
    data = json.loads(request.data)
    if 'attacker_id' not in data or 'defender_id' not in data:
        return bad_request('request body must include attacker_id and defender_id fields')
    if data['attacker_id'] == data['defender_id']:
        return bad_request('attacker and defender should not be the same')
    if not Player.query.filter_by(id=data['attacker_id']).first():
        return bad_request('attacker does not exists')
    if not Player.query.filter_by(id=data['defender_id']).first():
        return bad_request('defender does not exists')

    battle = Battle()
    battle.from_dict(data)
    db.session.add(battle)
    db.session.commit()

    return battle.to_dict()
