import random

from app import create_app, db
from app.battles.models import Battle
from app.players.models import Player

app = create_app()
app.app_context().push()


def list_of_not_processed_battles():
    ret = Battle.query.filter_by(started=False).order_by(Battle.id)
    return ret


def process_battle(battle):
    # random choice of winner
    choices = [battle.attacker_id, battle.defender_id]
    winner = random.choice(choices)
    loser = choices.pop(winner)[0]
    # set the winner
    battle.winner_id = winner
    # subtract gold from losing player
    loser_player = Player.query.get(loser)
    loser_gold_amount = loser_player.gold_amount
    stolen_amount_low = loser_gold_amount * 0.1
    stolen_amount_high = loser_gold_amount * 0.2
    stolen_amount = random.randrange(stolen_amount_low, stolen_amount_high)
    loser_player.gold_amount = loser_gold_amount - stolen_amount
    # add gold to winner
    winner_player = Player.query.get(winner)
    winner_player.gold_amount += stolen_amount
    db.session.add(battle)
    db.session.add(loser_player)
    db.session.add(winner_player)
    db.session.commit()


if __name__ == "__main__":
    battles = list_of_not_processed_battles()
    for instance in battles:
        process_battle(instance)
