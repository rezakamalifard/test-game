import unittest

from flask import json

from app import create_app, db
from app.players.models import Player
from config import TestConfig


class PlayerTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app(TestConfig)
        self.client = self.app.test_client

        with self.app.app_context():
            # create all tables for testing
            db.create_all()

    def test_create_player_fails_without_name(self):
        data = {}
        res = self.client().post('/players', data=data)
        self.assertEqual(res.status_code, 400)

    def test_create_player_fails_long_name(self):
        long_name = 'long name' * 10
        res = self.client().post('/players', data=json.dumps(dict(name=long_name)))
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.json['error'], 'Bad Request')
        self.assertIn(res.json['message'], 'max lenght for name field is 20')

    def test_create_player_fails_with_duplicated_name(self):
        data = data = {
            'name': 'player name',
            'gold_amount': 100,
            'attack_value': 200,
            'hit_points': 300,
            'luck_value': 400,
        }
        player = Player()
        player.from_dict(data)
        with self.app.app_context():
            db.session.add(player)
            db.session.commit()

        res = self.client().post('/players', data=json.dumps(data))

        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.json['error'], 'Bad Request')
        self.assertIn(res.json['message'], 'player name is already exists')

    def test_create_player_fails_with_duplicated_name_case_insensitively(self):
        data = {
            'name': 'player name',
            'gold_amount': 100,
            'attack_value': 200,
            'hit_points': 300,
            'luck_value': 400,
        }
        player = Player()
        player.from_dict(data)
        with self.app.app_context():
            db.session.add(player)
            db.session.commit()

        res = self.client().post('/players', data=json.dumps(dict(name='plaYer Name')))

        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.json['error'], 'Bad Request')
        self.assertIn(res.json['message'], 'player name is already exists')

    def test_create_player_success(self):
        data = {
            'name': 'player name',
            'gold_amount': 100,
            'attack_value': 200,
            'hit_points': 300,
            'luck_value': 400,
        }
        res = self.client().post('/players', data=json.dumps(data))

        expected = {
            'id': 1,
            'name': 'player name',
            'gold_amount': 100,
            'attack_value': 200,
            'hit_points': 300,
            'luck_value': 400,
        }

        response = res.json
        created_at = response.pop('created_at')
        self.assertEqual(res.status_code, 200)
        self.assertIsNotNone(created_at)
        self.assertDictEqual(response, expected)

    def test_get_leaderboard(self):
        player1_data = {
            'name': 'player 1',
            'gold_amount': 10,
        }
        player2_data = {
            'name': 'player 2',
            'gold_amount': 100,
        }
        player1 = Player()
        player2 = Player()
        player1.from_dict(player1_data)
        player2.from_dict(player2_data)
        with self.app.app_context():
            db.session.add(player1)
            db.session.add(player2)
            db.session.commit()

        res = self.client().get('/players/leaderboard')
        self.assertEqual(len(res.json), 2)
        self.assertEqual(res.json['1']['name'], 'player 2')
        self.assertEqual(res.json['1']['gold_amount'], 100)

        self.assertEqual(res.json['2']['name'], 'player 1')
        self.assertEqual(res.json['2']['gold_amount'], 10)


# Make the tests executable
if __name__ == "__main__":
    unittest.main()
