import os

basedir = os.path.abspath(os.path.dirname(__file__))

APP_NAME = "test_game"
DEFAULT_DATABASE_PATH = 'sqlite:///' + os.path.join(basedir, 'test_game.db')


class Config(object):
    DEBUG = True

    SQLALCHEMY_DATABASE_URI = os.getenv('TEST_GAME_DATABASE_URL', DEFAULT_DATABASE_PATH)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    INSTALLED_BLUEPRINTS = (
        'players',
        'battles',
        'leaderboards'
    )

    # Logging configs
    LOGGING_PATH = os.getenv('TEST_GAME_LOG_PATH', 'logs')
    LOGGING_FILE_NAME = APP_NAME + '.log'
    LOGGING_ADDRESS = LOGGING_PATH + LOGGING_FILE_NAME
    LOGGING_FORMAT = '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'


class DeploymentConfig(Config):
    DEBUG = False
    TESTING = False
    DEPLOYMENT = True


class DevelopmentConfig(Config):
    DEBUG = True


class TestConfig(Config):
    DEBUG = True
    TESTING = True

    SQLALCHEMY_DATABASE_URI = 'sqlite://'
