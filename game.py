# create WSGI app instance to server using flask
from app import create_app

app = create_app()
